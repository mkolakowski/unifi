#!/bin/bash
#--------------------------------------------------------------------------------------------------------------------
versionnumber=1.0.0
# Change 1.0.0
# - Inital Release of Script
#
# change 1.2.0
# - Added DuckDNS Support
#--------------------------------------------------------------------------------------------------------------------
echo " "
echo "  ___ ___      .__________.__"
echo " |   |   |____ |__\_  ____/__|   (c) 2010-2019"
echo " |   |   /    \|  ||  __) |  |   Ubiquiti Networks, Inc."
echo " |   |  |   |  \  ||  \   |  |   Current Version: " $(dpkg-query -W --showformat='${Version}\n' unifi)
echo " |______|___|  /__||__/   |__|   https://www.ui.com/"
echo "            |_/               "
echo " "
echo "Script Created and maintained by Matthew Kolakowski"
echo "Project lives at https://gitlab.com/mkolakowski/unifi/"
echo "------------------------------------------------------"
echo "Running Prescript checks"
echo "------------------------"
#--------------------------------------------------------------------------------------------------------------------
# Declaring Global Variables
    configfilelocation=/root/.config/unifiscript/unifiscript.conf
    cronreboottime=0
    cronupdateappsostime="0 6 * * *"
    cronbackuptime=0
    updatedhostname=0
    unifiurl=0
    letsencryptemail=0
    letsencryptunifi=/opt/gen-unifi-cert.sh
    newtimezone=America/New_York
    backuplocation01=0
    backuplocation02=0
    backupscriptconfig=true
    backuprcloneconfig=false
    duckdnstoken=0
# the below variables are not in the config file
    upgradescript=false
    useduckdns=false
    guidedconfigtemp=0
#--------------------------------------------------------------------------------------------------------------------
# Start time of the script
starttime=$(date +%Y%m%d-%H%M%S)
#--------------------------------------------------------------------------------------------------------------------
function functionCreateConfig () {
    if [[ -f  $configfilelocation ]]; 
        then
            echo "Removing existing config"
            echo "------------------------"
            rm $configfilelocation
        fi
    echo "Creating config at $configfilelocation"
    echo "-------------------------------------------------------------"
    echo "
    # Default Set to $configfilelocation, Stores the script config file location
            confconfigfilelocation=$configfilelocation
    # Default not set, stores new hostname
            confupdatedhostname=$updatedhostname
    # Default not set, ex 0 7 * * 1  will set the system to auto reboot at 0700 on monday(needs to be in parentheses)
            confcronreboottime='$cronreboottime'
    # Default Set to $cronupdateappsostime, is set to run apt update at 0600 daily
            confcronupdateappsostime='$cronupdateappsostime'
    # Default Set to $cronbackuptime, is set to run apt update at 0600 daily
            confcronbackuptime='$cronbackuptime'
    # Default not set, Stores the unifi URL if using duckdns pace whole url, example: exampledomain.duckdns.org
            confunifiurl=$unifiurl
    # Default not set, Stores email for letsencrypt
            confletsencryptemail=$letsencryptemail
    # Default Set to $letsencryptunifi, Stores location of letsencrypt unifi script
            confletsencryptunifi=$letsencryptunifi
    # Default Set to $newtimezone, Holds new timezone, See link for more zones https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
            confnewtimezone=$newtimezone
    # Default not set, Stores primary backup location
            confbackuplocation01=$backuplocation01
    # Default not set, Stores secondary backup location
            confbackuplocation02=$backuplocation02
    # Default Set to $backupscriptconfig, Stores true or false to backing up script config
            confbackupscriptconfig=$backupscriptconfig
    # Default Set to $backuprcloneconfig, Stores true or false to backing up rclone config
            confbackuprcloneconfig=$backuprcloneconfig
    # Default Set to $duckdnstoken, Stores the token obtained from https://www.duckdns.org/domains
            confduckdnstoken=$duckdnstoken
    # end
    " >> $configfilelocation
}
#--------------------------------------------------------------------------------------------------------------------
function functionLoadConfigVariables () {
    #tells the program to refrence this file
    . $configfilelocation
    echo "Loading Configiguration from file"
    echo "---------------------------------"
    configfilelocation=$confconfigfilelocation
    updatedhostname=$confupdatedhostname    
    cronreboottime=$confcronreboottime
    cronupdateappsostime=$confcronupdateappsostime
    cronbackuptime=$confcronbackuptime
    unifiurl=$confunifiurl
    letsencryptemail=$confletsencryptemail
    letsencryptunifi=$confletsencryptunifi
    newtimezone=$confnewtimezone
    backuplocation01=$confbackuplocation01
    backuplocation02=$confbackuplocation02
    backupscriptconfig=$confbackupscriptconfig
    backuprcloneconfig=$confbackuprcloneconfig
    duckdnstoken=$confduckdnstoken  
}
#--------------------------------------------------------------------------------------------------------------------
# Reads config and asks user if they want to modify it
function functionPrintConfig() {
    echo "      configfilelocation:    $configfilelocation"
    echo "      updatedhostname:       $updatedhostname"
    echo "      cronreboottime:        $cronreboottime"
    echo "      cronupdateappsostime:  $cronupdateappsostime"
    echo "      cronbackuptime:        $cronbackuptime"
    echo "      unifiurl:              $unifiurl"
    echo "      letsencryptemail:      $letsencryptemail"
    echo "      letsencryptunifi:      $letsencryptunifi"
    echo "      newtimezone:           $newtimezone"
    echo "      backuplocation01:      $backuplocation01"
    echo "      backuplocation02:      $backuplocation02"
    echo "      backupscriptconfig:    $backupscriptconfig"
    echo "      backuprcloneconfig:    $backuprcloneconfig"
    echo "      duckdnstoken:          $duckdnstoken"    
}
#--------------------------------------------------------------------------------------------------------------------
funcctionEditVariableGuided(){
    echo "----------------------------------------------------------"
    echo "configfilelocation is currently set to $configfilelocation"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            configfilelocation=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "cronreboottime is currently set to $cronreboottime"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            cronreboottime=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "cronupdateappsostime is currently set to $cronupdateappsostime"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            cronupdateappsostime=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "cronbackuptime is currently set to $cronbackuptime"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            cronbackuptime=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "updatedhostname is currently set to $updatedhostname"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            updatedhostname=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "unifiurl is currently set to $unifiurl"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            unifiurl=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "letsencryptemail is currently set to $letsencryptemail"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            letsencryptemail=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "letsencryptunifi is currently set to $letsencryptunifi"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            letsencryptunifi=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "newtimezone is currently set to $newtimezone"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            newtimezone=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "backuplocation01 is currently set to $backuplocation01"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            backuplocation01=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "backuplocation02 is currently set to $backuplocation02"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            backuplocation02=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "backupscriptconfig is currently set to $backupscriptconfig"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            backupscriptconfig=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "backuprcloneconfig is currently set to $backuprcloneconfig"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            backuprcloneconfig=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    echo "duckdnstoken is currently set to $duckdnstoken"
    read -p "  Please type the updated variable then press enter or enter 0 to skip:" guidedconfigtemp
    if [[ ! $guidedconfigtemp = 0 ]];
        then
            duckdnstoken=$guidedconfigtemp
            functionLoadConfigVariables
            echo "Variable updated"
        fi
        guidedconfigtemp=0
    echo "----------------------------------------------------------"
    functionCreateConfig
    functionLoadConfigVariables
    functionPrintConfig
}
#--------------------------------------------------------------------------------------------------------------------
function functionUpdateScript(){
    if [[ $upgradescript = true ]];
        then
            if [[ ! -f ./unifi.sh ]]; 
                then
                    echo "No Script to remove"
                    echo "-------------------"
                else
                    echo "Removing old script"
                    echo "-------------------"
                    sudo rm ./unifi.sh
                fi
        fi
    if [[ ! -f ./unifi.sh ]]; 
    then
        wget -q https://gitlab.com/mkolakowski/unifi/raw/master/unifi.sh
        if [[ $? -ne 0 ]];
            then
                echo "There was an error downloading the latest release"
                echo "-------------------------------------------------"
            else
                echo "Latest release Downloaded"
                echo "-------------------------"
            fi
        sudo chmod u+x ./unifi.sh
    fi
}
#--------------------------------------------------------------------------------------------------------------------
# Checks if a ConJob to set the timezone exists and to create it if it does not
function functionTimeZoneCron () {
    if  crontab -l | grep -q '#Set Timezone'
        then 
            echo "No Modifications Made for Timezone Cronjob"
            echo "-------------------------------------------"
        else 
            echo "Adding Cronjob to Set Timezone"
            echo "-------------------------------"
            crontab -l > /tmp/settimezonecron
            echo "#Set Timezone" >> /tmp/settimezonecron
            echo "      @reboot cp /usr/share/zoneinfo/$newtimezone /etc/localtime" >> /tmp/settimezonecron
            crontab /tmp/settimezonecron
            rm /tmp/settimezonecron
        fi
}
#--------------------------------------------------------------------------------------------------------------------
# Checks if a ConJob to set the timezone exists and to create it if it does not
function functionSystemRebootCron () {
    if [[ ! -z $cronreboottime ]];
        then
            if  crontab -l | grep -q '#System Reboot'
                then 
                    echo "No Modifications Made for System Reboot Cronjob"
                    echo "-------------------------------------------"
                else 
                    echo "Adding Cronjob to Set System Reboot"
                    echo "-------------------------------"
                    crontab -l > /tmp/systemrebootcron
                    echo "#System Reboot" >> /tmp/systemrebootcron
                    echo "      $cronreboottime reboot" >> /tmp/systemrebootcron
                    crontab /tmp/systemrebootcron
                    rm /tmp/systemrebootcron
                fi
            fi
}
#--------------------------------------------------------------------------------------------------------------------
# Checks if a ConJob to Automaticly update the System and Apps exists and to create it if it does not
function functionSystemUpdateCron () {
    if [[ ! -z $cronupdateappsostime ]];
        then
        if  crontab -l | grep -q '#Update Apps and OS'
            then 
                echo "No Modifications Made for System Update Cronjob"
                echo "-------------------------------------------"
            else 
                echo "Adding Cronjob to Automaticly update the System and Apps"
                echo "-------------------------------"
                crontab -l > /tmp/SystemUpdatecron
                echo "#Update Apps and OS" >> /tmp/SystemUpdatecron
                echo "      $cronupdateappsostime apt-get update && apt-get upgrade -f --assume-yes" >> /tmp/SystemUpdatecron
                crontab /tmp/SystemUpdatecron
                rm /tmp/SystemUpdatecron
            fi
        fi
}
#--------------------------------------------------------------------------------------------------------------------
# Installs and sets up the letsencrypt certificate creation and renewal process conjob
function functionUnifiLetsEncryptCron () {
    if  crontab -l | grep -q '#LetsEncrypt Job'
    then 
        echo "No Modifications Made for LetsEncrypt Cron"
        echo "------------------------------------------"
    else 
        echo "Adding Cronjob for LetsEncrypt Certificate Renewal"
        echo "--------------------------------------------------"
        crontab -l > /tmp/letsencryptcron
        echo "#LetsEncrypt Job" >> /tmp/letsencryptcron
        echo "      0 0 * * 0 $letsencryptunifi -r -d $unifiurl" >> /tmp/letsencryptcron
        crontab /tmp/letsencryptcron
        rm /tmp/letsencryptcron
    fi
}
#--------------------------------------------------------------------------------------------------------------------
# Checks if a ConJob to run a backup job exists and to create it if it does not
function functionBackupCron () {
    if [[ ! -z $cronupdateappsostime ]];
        then
            if  crontab -l | grep -q 'Unifi Backup Job'
                then 
                    echo "No Modifications Made for unifi backup cron"
                    echo "-------------------------------------------"
                else
                    echo "Adding Cronjob for unifi backup"
                    echo "-------------------------------"
                    crontab -l > /tmp/unifibackupcron
                    echo "#Unifi Backup Job" >> /tmp/unifibackupcron
                    #edit below to change the time of the backup
                    echo "      $cronbackuptime /root/unifi.sh backup" >> /tmp/unifibackupcron
                    crontab /tmp/unifibackupcron
                    rm /tmp/unifibackupcron
                fi
        fi
}
#--------------------------------------------------------------------------------------------------------------------
function functionupdateosapps () {
    echo "Performing updates"
    echo "------------------"
    apt-get update && apt-get upgrade -f --assume-yes
    upgradescript=true
    functionUpdateScript
}
#--------------------------------------------------------------------------------------------------------------------
function functionSetHostname () {
    echo "Changing hostname to $updatedhostname"
    echo "--------------------"
    sudo hostname "$updatedhostname"
    sudo hostnamectl set-hostname "$updatedhostname"
}
#--------------------------------------------------------------------------------------------------------------------
# Reads config and asks user if they want to modify it
function functionReadConfig() {
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo "!! Please verify that the config is correct before proceding  !!"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    read -p "Do you want to edit the config before proceding? (y/n)" boolconfigedit
    echo "------------------------------------------------------"
    if [[ $boolconfigedit == "y" ]] || [[ $boolconfigedit == "Y" ]];
        then
            echo "Launching text editor"
            echo "---------------------"
            nano $configfilelocation
            functionLoadConfigVariables
            functionPrintConfig
        else 
            echo "No changes made to configuration file"
            echo "-------------------------------------"
        fi
}
#--------------------------------------------------------------------------------------------------------------------
#stores config data and re-creates config with user data
function functionReloadConfig (){
    read -p "Are you sure you want to rebuild the config? (y/n)" boolconfigreload
    echo    "------------------------------------------------------"
    if [[ $boolconfigreload == "y" ]] || [[ $boolconfigreload == "Y" ]];
        then
            functionLoadConfigVariables #Loading the latest variables
            cp $configfilelocation $configfilelocation-$starttime.OLD
            echo "Backup Config located at: $configfilelocation-$starttime.OLD"
            echo "------------------------"
            rm $configfilelocation
            functionCreateConfig
        else 
            echo "No changes made to configuration file"
            echo "-------------------------------------"
        fi
}
#--------------------------------------------------------------------------------------------------------------------
# Runs though a firstime setup for the unifi controller
function functionUnifiInstall() {
    # Updates the OS
    echo "Updating OS"
    echo "-----------"
    apt-get update
    apt-get upgrade -y
    apt-get install apt-transport-https -y
    echo "Changing hostname to $updatedhostname"
    echo "--------------------"
    sudo hostname "$updatedhostname"
    sudo hostnamectl set-hostname "$updatedhostname"
    echo "Installing Unifi"
    echo "----------------"
    echo 'deb http://www.ui.com/downloads/unifi/debian stable ubiquiti' | sudo tee /etc/apt/sources.list.d/100-ubnt-unifi.list
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
    echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
    sudo apt update
    sudo wget -q -O /etc/apt/trusted.gpg.d/unifi-repo.gpg https://dl.ui.com/unifi/unifi-repo.gpg 
    sudo apt install unifi -y
    functionTimeZoneCron
    functionSystemUpdateCron
}
#--------------------------------------------------------------------------------------------------------------------
# Installs NGINX reverse proxy to redirect http request to https
function functionunifiNGINXtSetup () {
    echo "Installing Nginx"
    echo "----------------"
    sudo apt install nginx-light -y
    echo "Configuring Nginx to forward HTTP to HTTPS"
    echo "------------------------------------------"
    echo "server {
            listen 80 default_server;
            listen [::]:80 default_server;
            server_name _;
            return 301 https://\$host\$request_uri;
    }" > /etc/nginx/sites-available/redirect
    ln -s /etc/nginx/sites-available/redirect /etc/nginx/sites-enabled/
    rm /etc/nginx/sites-enabled/default
}
#--------------------------------------------------------------------------------------------------------------------
# Installs and sets up the letsencrypt certificate creation and renewal process conjob
function functionunifiLetsEncryptSetup () {
    echo "Installing LetsEncrypt"
    echo "----------------------"
    apt-get install letsencrypt -y
    # Setup LetsEncrypt certificate
    # from https://cloud.linode.com/stackscripts/348247 
    echo "Downloading LetsEncrypt Script to ${letsencryptunifi}"
    echo "---------------------------------------------------"
    wget -q -O /opt/gen-unifi-cert.sh https://source.sosdg.org/brielle/lets-encrypt-scripts/raw/master/gen-unifi-cert.sh
    sed -i 's/--agree-tos --standalone --preferred-challenges tls-sni/--agree-tos --standalone/g' /opt/gen-unifi-cert.sh
    sed -i -e '/PATH=/a\' -e 'service nginx stop' /opt/gen-unifi-cert.sh
    sed -i -e "\$aservice nginx start" /opt/gen-unifi-cert.sh
    sudo chmod u+x  /opt/gen-unifi-cert.sh
    yes no | /opt/gen-unifi-cert.sh -e $letsencryptemail -d "$unifiurl"
    functionUnifiLetsEncryptCron
}
#--------------------------------------------------------------------------------------------------------------------
# Installs and sets up Fail2ban and adds a jail for unifi
function functionunifiFail2BanSetup () {
    # Fail2Ban needs three files and a reload
    # from https://metis.fi/en/2018/02/unifi-on-gcp/
    echo "Installing Fail2Ban"
    echo "-------------------"
    f2b=$(dpkg-query -W --showformat='${Status}\n' fail2ban 2>/dev/null)
    if [[ "x${f2b}" != "xinstall ok installed" ]]; then 
        if apt-get -qq install -y fail2ban >/dev/null; then
                echo "Fail2Ban installed"
                echo "------------------"
        fi
        if [ ! -f /etc/fail2ban/filter.d/unifi-controller.conf ]; then
            echo "Creating Fail2Ban Filter for Unifi"
            echo "----------------------------------" 
                echo "[Definition]" >> /etc/fail2ban/filter.d/unifi-controller.conf
                echo "#failregex = ^.* Failed .* login for .* from <HOST>\s*$" >> /etc/fail2ban/filter.d/unifi-controller.conf
            echo "Creating Fail2Ban Jail for Unifi"
            echo "----------------------------------" 
                echo "[unifi-controller]" >> /etc/fail2ban/jail.d/unifi-controller.conf
                echo "filter   = unifi-controller" >> /etc/fail2ban/jail.d/unifi-controller.conf
                echo "port     = 8443" >> /etc/fail2ban/jail.d/unifi-controller.conf
                echo "logpath  = /var/log/unifi/server.log" >> /etc/fail2ban/jail.d/unifi-controller.conf
	fi
    # The .local file will be installed in any case
        echo "[unifi-controller]" >> /etc/fail2ban/jail.d/unifi-controller.local
        echo "enabled  = true" >> /etc/fail2ban/jail.d/unifi-controller.local
        echo "maxretry = 3" >> /etc/fail2ban/jail.d/unifi-controller.local
        echo "bantime  = 3600" >> /etc/fail2ban/jail.d/unifi-controller.local
        echo "findtime = 3600" >> /etc/fail2ban/jail.d/unifi-controller.local
	systemctl reload-or-restart fail2ban
    fi
}
#--------------------------------------------------------------------------------------------------------------------
# Sets up a UFW firewall
function functionfirewallsetup () {
    echo "Installing UFW Firewall"
    echo "-----------------------"
        sudo apt install ufw -y
    echo "Setting up UFW Firewall Rules"
    echo "-----------------------------"
        sudo ufw allow ssh
        sudo ufw allow 80/tcp
        sudo ufw allow 22/tcp
        sudo ufw allow 443/tcp
        sudo ufw allow 8080/tcp
        sudo ufw allow 8443/tcp
        sudo ufw allow 8843/tcp
        sudo ufw allow 3478/udp
        #sudo ufw allow 'Nginx HTTPS'
}
#--------------------------------------------------------------------------------------------------------------------
# runs a backup of the unifi config, options to backup more
function functionunifiBackup () {
    echo "Starting Backup"
    echo "---------------"
    #variable - pulls version of Unifi Controller and sets 
    unifiversion=$(dpkg -l | awk '$2=="unifi" { print substr ($3,0, ( match ( $3,"-" ) ) -1 ) } ' )
    #'
    echo $unifiversion
    echo "Copying Unifi Backups to $backuplocation01"
    echo "------------------------------------------"
    rclone copy -P --include autobackup_$unifiversion*.unf /var/lib/unifi/backup/autobackup $backuplocation01/Controller/$unifiversion
    echo "Copying Unifi Backups to $backuplocation02"
    echo "------------------------------------------"
    rclone copy -P --include autobackup_$unifiversion*.unf /var/lib/unifi/backup/autobackup $backuplocation02/Controller/$unifiversion
    echo "Backing up script config to $backuplocation01"
    echo "---------------------------------------------"
    rclone copy -P $configfilelocation $backuplocation01/scriptconfig/$starttime
    echo "Backing up script config to $backuplocation02"
    echo "---------------------------------------------"
    rclone copy -P $configfilelocation $backuplocation02/scriptconfig/$starttime
    # Create crontab for Unifi Backup Script if one does not already exist
    functionBackupCron
}
#--------------------------------------------------------------------------------------------------------------------
# runs a backup of the unifi config, options to backup more
function functionRestoreBackup () {
    echo "stub"
    # ask for location
    # verifiy file exists and ask if location is not valid (for loop)
    # download then extract into temp then copy to correct locations
}
#--------------------------------------------------------------------------------------------------------------------
#Creates a MOTD for the application server
function functionCreateMOTD (){
    unifimotdLocation=/etc/update-motd.d/01-unifi-motd
    sudo apt install screenfetch -y
    echo "Disabling all current MOTDs"
    echo "---------------------------"
    sudo chmod -x /etc/update-motd.d/*
    echo "Creating Custom MOTD"
    echo "-----------------"
    wget "https://gitlab.com/mkolakowski/unifi/raw/master/01-unifi-motd.sh" -O $unifimotdLocation
    chmod +x $unifimotdLocation
}
#--------------------------------------------------------------------------------------------------------------------
function functionDuckDNSInstall (){
    if [[ $useduckdns = true ]];
        then
            if [[ $duckdnstoken = 0 ]];
                then
                    echo "Please navigate to https://www.duckdns.org/domains to get your duckdns token"
                    read -p "Enter your DuckDNS Token here:" duckdnstokentemp
                    duckdnstoken=$duckdnstokentemp
                    functionCreateConfig
                    functionLoadConfigVariables
                    echo "duckdnstoken is now set to: $duckdnstoken"
                    echo "---------------------------"
                fi
            if [[ $unifiurl = 0 ]];
                then
                    echo "Please navigate to https://www.duckdns.org/domains to get your duckdns URL"
                    read -p "Enter your DuckDNS URL here:" unifiurltemp
                    unifiurl=$unifiurltemp
                    functionCreateConfig
                    functionLoadConfigVariables
                    echo "unifiurl is now set to: $unifiurl"
                    echo "-----------------------"
                fi
            if [[ ! -f /root/duckdns/ ]]; 
                then
                    mkdir -p /root/duckdns
                fi
            if [[ ! -f /root/duckdns/duck.sh ]]; 
                then
                    touch /root/duckdns/duck.sh
                    echo 'echo url="https://www.duckdns.org/update?domains='$unifiurl'&token='$duckdnstoken'&ip=" | curl -s -k -o /root/duckdns/duck.log -K -' >> /root/duckdns/duck.sh
                    sudo chmod u+x /root/duckdns/duck.sh
                    /root/duckdns/duck.sh
                else
                    read -p "!!!Warning!!! this distroys the duck dns script, do you want to continue? (y/n)" boolduckdns
                    echo    "------------------------------------------------------"
                    if [[ $boolduckdns == "y" ]] || [[ $boolduckdns == "Y" ]];
                        then
                            rm /root/duckdns/duck.sh
                            echo 'echo url="https://www.duckdns.org/update?domains='$unifiurl'&token='$duckdnstoken'&ip=" | curl -s -k -o /root/duckdns/duck.log -K -' >> /root/duckdns/duck.sh
                            sudo chmod u+x /root/duckdns/duck.sh
                            /root/duckdns/duck.sh
                        fi
                fi
            fi
}
#--------------------------------------------------------------------------------------------------------------------
# Downloads Fresh script if n0ne present
    upgradescript=false
    functionUpdateScript
#--------------------------------------------------------------------------------------------------------------------
# Config Loader/Generator
if [[ ! -f  $configfilelocation ]]; 
    then
        if [[ ! -f /root/.config/unifiscript ]];
        then
            if [[ ! -f /root/.config ]];
            then
                sudo mkdir /root/.config
                sudo mkdir /root/.config/unifiscript
                echo "Creating Config Directory"
                echo "-------------------------"
            else
                sudo mkdir /root/.config/unifiscript
                echo "Creating Config Directory"
                echo "-------------------------"
            fi
        else
            echo "Config Directory Exists"
            echo "-----------------------"
        fi
        #Creates Config
        functionCreateConfig
    fi
    functionLoadConfigVariables
    functionPrintConfig
#--------------------------------------------------------------------------------------------------------------------
# Takes user input from script and runs it and will display help if no commands are recognized
if [[ "$1" == "install" ]];
    then
        functionupdateosapps
        functionReadConfig
#        functioninstallrclone
        echo "Installing Unifi Server"
        echo "-----------------------"
        functionDuckDNSInstall
        functionUnifiInstall
        #echo "Installing Nginx"
        #echo "----------------"
        #functionunifiNGINXtSetup
        echo "Installing LetsEncrypt"
        echo "----------------------"
        functionunifiLetsEncryptSetup
        echo "Installing Fail2Ban"
        echo "-------------------"
        functionunifiFail2BanSetup
        echo "Configuring the firewall"
        echo "------------------------"
        functionfirewallsetup
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "backup" ]];
        then
            functionunifiBackup
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "config" ]];
        then
            functionReadConfig
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "cron" ]];
        then
            functionTimeZoneCron
            functionUnifiLetsEncryptCron
            functionBackupCron
            functionSystemUpdateCron
            functionSystemRebootCron
            echo "Printing Contab"
            echo "---------------"
            sudo crontab -u root -l
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "duckdns" ]];
        then
            useduckdns=true
            functionDuckDNSInstall
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "guided" ]];
        then
            funcctionEditVariableGuided "$hostname"
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "hostname" ]];
        then
            functionSetHostname
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "motd" ]];
        then
            functionCreateMOTD
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "reload" ]];
        then
            functionReloadConfig
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "restart" ]];
        then
            echo "Restarting Unifi Server"
            sudo service unifi restart
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "restore" ]];
        then
            functionRestoreBackup
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "start" ]];
        then
            echo "starting Unifi Server"
            sudo service unifi start
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "stop" ]];
        then
            echo "Stopping Unifi Server"
            sudo service unifi stop
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "update" ]];
        then
            functionupdateosapps
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "updateself" ]];
        then
            upgradescript=true
            functionUpdateScript
    #--------------------------------------------------------------------------------------------------------------------
    else
        echo " "
        echo " |------------|-------No command found. Here is a list of commands------------------------------------|"
        echo " | backup     | Copies the Unifi Generated Backups to a destination of your choice                    |"
        echo " | config     | Prints Script Config on screen and gives option to edit in nano                       |"
        echo " | cron       | Checks if Cron Jobs are in place and prints entire root Contab                        |"
        echo " | duckdns    | Installs and sets up DuckDNS                                                          |"
        echo " | guided     | Performes a Guided setup                                                              |"
        echo " | install    | Performs the install of the Unifi Controller, LetsEncrypt, Fail2Ban and UFW Firewall  |"
        echo " | hostname   | Updates hostname of system to the listed in the configuration file                    |"
        echo " | motd       | Installs a custom MOTD for the unifi controller                                       |"
        echo " | reload     | Renames current config and creates a new config with users variables                  |"
        echo " | restart    | Performs a Restart on the Unifi Controller Service                                    |"
        echo " | restore    | Copies Files from the backup location to restore the Script Config                    |"
        echo " | start      | Performs a manual start of the Unifi Software                                         |"
        echo " | stop       | Performs a hard stop of the Unifi Controller Software                                 |"
        echo " | update     | Patches applications and OS                                                           |"
        echo " | updateself | Deletes and downloads the latest version of this script                               |"
        echo " |------------|---------------------------------------------------------------------------------------|"
        echo " "
        fi
    #--------------------------------------------------------------------------------------------------------------------
