# unifi

All in one script to install, maintain, backup a Unifi Controller

**Latest Release** 

https://gitlab.com/mkolakowski/unifi/-/releases


**Bleeding Edge Release** (may have lots of issues)

`curl -s https://gitlab.com/mkolakowski/unifi/raw/master/unifi.sh | sudo bash`


**Cloud Provider Referal**

`https://m.do.co/c/66b299a1d459` or 'https://www.linode.com/?r=3e616e55452463fb02af5d5d1ebd59c0e01c861c'